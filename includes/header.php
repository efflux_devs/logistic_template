<header class="header">
	<nav class="top-bar">
		<div class="overlay-bg">
			<div class="container">
				<div class="row">
					
					<div class="col-sm-6 col-xs-12">
  					<div class="call-to-action">
  						<ul class="list-inline">
  							<li><a href="#"><i class="fa fa-phone"></i> 1-800-987-654</a></li>
  							<li><a href="#"><i class="fa fa-envelope"></i> admin@domain.com</a></li>
  						</ul>
  					</div><!-- /.call-to-action -->
					</div><!-- /.col-sm-6 -->

					<div class="col-sm-6 hidden-xs">
  					<div class="topbar-right">
	  					<div class="lang-support pull-right">
							<select class="cs-select cs-skin-elastic">
								<option value="" disabled selected>Language</option>
								<option value="united-kingdom" data-class="flag-uk">English</option>
								<option value="france" data-class="flag-france">French</option>
								<option value="spain" data-class="flag-spain">Spanish</option>
								<option value="south-africa" data-class="flag-bd">Bengali</option>
							</select>
						</div>

  						<ul class="social-links list-inline pull-right">
  							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
  							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
  							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
  							<li><a href="#"><i class="fa fa-tumblr"></i></a></li>
  						</ul>
  					</div><!-- /.social-links -->
					</div><!-- /.col-sm-6 -->
  				
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.overlay-bg -->
	</nav><!-- /.top-bar -->

	<div id="search">
	    <button type="button" class="close">×</button>
	    <form>
	        <input type="search" value="" placeholder="type keyword(s) here" />
	        <button type="submit" class="btn btn-primary">Search</button>
	    </form>
	</div>
	
	<nav class="navbar navbar-default" role="navigation">
		
		<div class="container mainnav">
			<div class="navbar-header">
				<h1 class="logo"><a class="navbar-brand" href="index.php"><img src="img/logol.png" alt="" height="40px;"></a></h1>

				<!-- offcanvas-trigger -->
                <button type="button" class="navbar-toggle collapsed pull-right" >
                  <span class="sr-only">Toggle navigation</span>
                  <i class="fa fa-bars"></i>
                </button>

			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-collapse">

				

                <span class="search-button pull-right"><a href="#search"><i class="fa fa-search"></i></a></span>

				<ul class="nav navbar-nav navbar-right">
					<!-- Home -->
                    <li class="dropdown active"><a href="index.php">Home </a>
                        <!-- submenu-wrapper -->
                        <!-- <div class="submenu-wrapper">
                            <div class="submenu-inner">
                                <ul class="dropdown-menu">
                                	<li><a href="index2.html">Home2</a></li>
                                </ul>
                            </div>
                        </div> -->
                        <!-- /submenu-wrapper -->
                    </li>
                    <!-- /Home -->

                    <!-- Pages -->
                    <li class="dropdown"><a href="#">Pages <span class="fa fa-angle-down"></span></a>
                        <!-- submenu-wrapper -->
                        <div class="submenu-wrapper">
                            <div class="submenu-inner">
                                <ul class="dropdown-menu">
                                	<li><a href="about.php">About</a></li>
                                    <li><a href="service.php">Service</a></li>
                                    <li><a href="our-people.php">Our people</a></li>
                                    <li><a href="faq.php">FAQ Page</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /submenu-wrapper -->
                    </li>
                    <!-- /Pages -->

                    <!-- Services -->
                    <li class="dropdown"><a href="#">Services <span class="fa fa-angle-down"></span></a>
                        <!-- submenu-wrapper -->
                        <div class="submenu-wrapper">
                            <div class="submenu-inner">
                                <ul class="dropdown-menu">
                                	<li><a href="air.php">Air transportation</a></li>
                                    <li><a href="marine.php">Marine transportation</a></li>
                                    <li><a href="moving.php">Moving & storage</a></li>
                                    <li><a href="shipping.php">Shipping & operations</a></li>
                                    <li><a href="transportation.php">Transportation logistics</a></li>
                                    <li><a href="trucking.php">Trucking</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /submenu-wrapper -->
                    </li>
                   
					<!-- Blog -->
                    <li class="dropdown"><a href="blog.php">Blog <span class="fa fa-angle-down"></span></a>
                        <!-- submenu-wrapper -->
                        <!-- <div class="submenu-wrapper">
                            <div class="submenu-inner">
                                <ul class="dropdown-menu">
                                	<li><a href="blog.html">Standard blog</a></li>
                                </ul>
                            </div>
                        </div> -->
                        <!-- /submenu-wrapper -->
                    </li>
					<li><a href="contact.php">Contact</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container -->							
	</nav>
</header>