<section class="footer-widget-section section-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-md-offset-1 col-sm-4">
				<div class="footer-widget">
					<h3>Place &amp; Contact</h3>

					<address>
					  3605 Benson Ave<br>
					  Zenith Carex USA, Baltimore MD 21227 USA<br>
					  <span class="tel">(+234) 703-211-4180</span>
					</address>


					<!-- Modal -->
					<div class="modal fade" id="cssMapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog modal-lg">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="myModalLabel">Our Location</h4>
					      </div>
					      <div class="modal-body">

						    <div id="googleMap"></div>
						    
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>

			<div class="col-md-3 col-sm-4">
				<div class="footer-widget">
					<h3>About Us</h3>

					<ul>
                    	<li><a href="about.php">About</a></li>
                        <li><a href="service.php">Service</a></li>
                        <li><a href="our-people.php">Our people</a></li>
                        <li><a href="career.php">Career</a></li>
                        <li><a href="faq.php">FAQ Page</a></li>
                    </ul>
				</div><!-- /.footer-widget -->
			</div><!-- /.col-md-4 -->

			<div class="col-md-4 col-sm-4">
				<div class="footer-widget">
					<h3>Stay in Touch</h3>
					<p>Enter your email address to receive news &amp; offers from us</p>

					<form class="newsletter-form">
						<div class="form-group">
							<label class="sr-only" for="InputEmail1">Email address</label>
							<input type="email" class="form-control" id="InputEmail1" placeholder="Your email address">
							<button type="submit" class="">Send &nbsp;<i class="fa fa-angle-right"></i></button>
						</div>
					</form>		        				
				</div>
			</div>
		</div>
	</div>
</section>

<footer class="copyright-section">
	<div class="container text-center">
		<div class="footer-menu">
			<ul>
				<li><a href="#">Privacy &amp; Cookies</a></li>
				<li><a href="#">Terms &amp; Conditions</a></li>
				<li><a href="#">Accessibility</a></li>
			</ul>
		</div>

		<div class="copyright-info">
			<span>Copyright © 2018 Zenith Carex. All Rights Reserved. Designed by <a href="http://greymatteragency.com/">Greymatter Agency</a></span>
		</div>
	</div>
</footer>