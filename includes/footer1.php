<div class="offcanvas-menu offcanvas-effect">
		<div class="offcanvas-wrap">
	        <div class="off-canvas-header">
	        	<button type="button" class="close" aria-hidden="true" data-toggle="offcanvas" id="off-canvas-close-btn">&times;</button>
	        </div>
	        <ul id ="offcanvasMenu" class="list-unstyled visible-xs visible-sm">
	            <li class="active"><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
	            <li>
	            	<a href="#">Pages</a>
	            	<ul>
						<li><a href="">About</a></li>
					    <li><a href="">Service</a></li>
					    <li><a href="">Our people</a></li>
					    <li><a href="">Career</a></li>
					    <li><a href="">FAQ Page</a></li>
					</ul>

	            </li>
	            <li>
	            	<a href="#">Services</a>
	            	<ul>
						<li><a href="">Moving &amp; storage</a></li>
						<li><a href="">Shipping &amp; operations</a></li>
						<li><a href="">Trucking</a></li>
					</ul>

	            </li>
	            <li>
	            	<a href="#">Blog</a>
					<ul>
	            		<li>
	            			<a href="">Standard blog</a>
	            		</li>
	            		<li>
	            			<a href="">Single blog</a>
	            		</li>
	            	</ul>
	            </li>
	            <li><a href="contact.php">Contact</a></li>
	        </ul>
	        <div class="offcanvas-widgets hidden-sm hidden-xs">
		        <div id="twitterWidget">
					<h2>Tweeter feed</h2>				    	
					<div class="twitter-widget"></div>
				</div>
				<div class="newsletter-widget">
					<h2>Stay in Touch</h2>
					<p>Enter your email address to receive news &amp; offers from us</p>

					<form class="newsletter-form">
						<div class="form-group">
							<label class="sr-only" for="InputEmail1">Email address</label>
							<input type="email" class="form-control" id="InputEmail2" placeholder="Your email address">
							<button type="submit" class="btn">Send &nbsp;<i class="fa fa-angle-right"></i></button>
						</div>
					</form>		
				</div>
			</div>
		</div>
  	</div>