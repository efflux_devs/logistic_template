<!DOCTYPE html>
<html lang="en">

	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Zenith Carex International Limited</title>
	    <!-- Web Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <!-- Bootstrap Core CSS -->
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <!-- Flaticon CSS -->
	    <link href="fonts/flaticon/flaticon.css" rel="stylesheet">
	    <!-- font-awesome CSS -->
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <!-- Offcanvas CSS -->
	    <link href="css/hippo-off-canvas.css" rel="stylesheet">
	    <!-- animate CSS -->
	    <link href="css/animate.css" rel="stylesheet">
	    <!-- language CSS -->
	    <link href="css/language-select.css" rel="stylesheet">
	    <!-- owl.carousel CSS -->
	    <link href="owl.carousel/assets/owl.carousel.css" rel="stylesheet">
		<!-- magnific-popup -->
    	<link href="css/magnific-popup.css" rel="stylesheet">
    	<!-- Main menu -->
    	<link href="css/menu.css" rel="stylesheet">
    	<!-- Template Common Styles -->
    	<link href="css/template.css" rel="stylesheet">
	    <!-- Custom CSS -->
	    <link href="css/style.css" rel="stylesheet">
	    <!-- Responsive CSS -->
	    <link href="css/responsive.css" rel="stylesheet">

	    <script src="js/vendor/modernizr-2.8.1.min.js"></script>
	    <!-- HTML5 Shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
		    <script src="js/vendor/html5shim.js"></script>
		    <script src="js/vendor/respond.min.js"></script>
	    <![endif]-->
	</head>


	<body id="page-top">
		<div id="st-container" class="st-container">
		    <div class="st-pusher">
	        	<div class="st-content">
				  	
				  	<?php
				       include ("includes/header.php");
				    ?>

                    <div id="main-carousel" class="carousel slide hero-slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#main-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#main-carousel" data-slide-to="1"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="img/slider/slide-1.jpg" alt="Hero Slide">
                                <!--Slide Image-->

                                <div class="container">
                                    <div class="carousel-caption">
                                        <small class="animated fadeIn">CALL US TODAY</small>
                                        <div class="phone animated lightSpeedIn">+234-703-211-4180</div>
                                        <h1 class="animated lightSpeedIn">Making transportation fast and safe</h1>
                                        <p class="lead animated lightSpeedIn">It's a tag line, where you can write a key point of your idea.
                                            It is a long
                                            established fact that a reader will be distracted.</p>

                                        <a class="btn btn-primary animated lightSpeedIn" href="#">Work With Us Today</a>
                                    </div>
                                    <!--.carousel-caption-->
                                </div>
                                <!--.container-->
                            </div>
                            <!--.item-->

                            <div class="item">
                                <img src="img/slider/slide-2.jpg" alt="Hero Slide">
                                <!--Slide Image-->

                                <div class="container">
                                    <div class="carousel-caption">

                                        <h1 class="animated bounceIn">We value your time and money</h1>

                                        <p class="lead animated bounceIn">It's a tag line, where you can write a key point of your idea. It
                                            is a long
                                            established fact that a reader will be distracted.</p>
                                        <a class="btn btn-primary animated bounceIn" href="#">Work With Us Today</a>
                                    </div>
                                    <!--.carousel-caption-->
                                </div>
                                <!--.container-->
                            </div>
                            <!--.item-->
                        </div>
                        <!--.carousel-inner-->

                        <!-- Controls -->
                        <a class="left carousel-control" href="#main-carousel" role="button" data-slide="prev">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#main-carousel" role="button" data-slide="next">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                  
					<section class="service-home section-padding">
			            <div class="container text-center">
			              <div class="row">
			                <div class="col-xs-12">
			                  <h2 class="section-title"> Our 24 Hour’s Services </h2>
			                  <span class="section-sub"> Nullam ac urna ey felis dapibus <br> condimeytum sit amet </span>
			                </div>
			              </div> <!-- /.row -->

			              <div class="row content-row">
			              	<div class="col-sm-4">
			              		<div class="service">
				              		<div class="service-thumb-home">
				              			<a href="#"><img src="img/trans.jpg" alt=""></a>
				              		</div>
				              		
				              		<h3>Courier</h3>
				              		<p>A Smart logistic solution to get merchandise delivered from USA with local customer service based in Nigeria. Zenith Carex USA Services is an easy solution </p>
				              		<a class="readmore" href="#">Read More &nbsp;<i class="fa fa-angle-right"></i> </a>
			              		</div>
			              	</div><!-- /.col-sm-4 -->

			              	<div class="col-sm-4">
			              		<div class="service">
				              		<div class="service-thumb-home">
				              			<a href="#"><img src="img/moving.jpg" alt=""></a>
				              		</div>
				              		<h3>Clearance &amp; Freight</h3>
				              		<p>offers customs clearance and final delivery services to every part of the country. Our well designed customs clearance program reduces cycle time, improve shipment visibility,</p>
				              		<a class="readmore" href="#">Read More &nbsp;<i class="fa fa-angle-right"></i> </a>
			              		</div>

			              	</div><!-- /.col-sm-4 -->

			              	<div class="col-sm-4">
			              		<div class="service">
				              		<div class="service-thumb-home">
				              			<a href="#"><img src="img/shipping.jpg" alt=""></a>
				              		</div>
				              		<h3>Haulage / Logistics</h3>
				              		<p>Zenith Carex International offers forwarding services in the field of freight transportation by road, air or sea. Our multi-modal haulage network is designed to offer a seamless transportation </p>
				              		<a class="readmore" href="#">Read More &nbsp;<i class="fa fa-angle-right"></i> </a>
			              	   </div>
			              	</div><!-- /.col-sm-4 -->
			              </div> <!-- /.row -->
			            </div><!-- /.container -->
			        </section>
			        
			        <section class="feature-section section-padding">
				        <div class="container">
				        	<div class="row">
				        		<div class="col-sm-7 col-xs-12">
				        			<h2>We do care about your cargo!</h2>

				        			<p>Our clientele base is widespread and cuts across the organized private sector, NGOs and Government parastatals and establishments nationwide. Being an international courier & logistics company our clientele base also spreads to countries in Asia, Europe and America. </p> 

									<!-- <p>Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p> -->

									<a href="#" class="btn btn-primary">View Details</a>
				        		</div>
				        
				        	</div>
				        </div>
			        </section>
			       
			        <section class="why-us-setion section-padding">
			        	<div class="container">
			              <div class="row text-center">
			                <div class="col-xs-12">
			                  <h2 class="section-title">Why Chose Us</h2>
			                  <span class="section-sub">Monotonectally innovate cross-media<br> resources without seamless</span>
			                </div>
			              </div> 

			              <div class="row content-row">
			              	<div class="col-md-12">
								<div class="css-tab" role="tabpanel">

								  <ul class="nav nav-tabs" role="tablist">
								    <li role="presentation" class="active"><a href="#secure" aria-controls="secure" role="tab" data-toggle="tab"><i class="flaticon-logistics15"></i> Secure</a></li>
								    <li role="presentation"><a href="#trackable" aria-controls="trackable" role="tab" data-toggle="tab"><i class="flaticon-logistics18"></i> Trackable</a></li>
								    <li role="presentation"><a href="#fast" aria-controls="fast" role="tab" data-toggle="tab"><i class="flaticon-logistics16"></i> Fast</a></li>
								    <li role="presentation"><a href="#reliable" aria-controls="reliable" role="tab" data-toggle="tab"><i class="flaticon-broken43"></i> Reliable</a></li>
								  </ul>

								  <div class="tab-content">
								    <div role="tabpanel" class="tab-pane active fade in" id="secure">
									    <div class="css-tab-content">
										    <div class="row">
										    	<div class="col-md-6">
										    		<img src="img/secure.png" alt="">
										    	</div><!-- /.col-md-6 -->

										    	<div class="col-md-6 content-text">
											    	<h3>Continually disintermediate</h3>

											    	<p>Credibly build virtual materials for fully researched paradigms. Authoritatively plagiarize long-term high-impact infomediaries with goal-oriented core competencies. Conveniently visualize process-centric applications through leading-edge testing procedures. Quickly build backend customer service for real-time metrics.</p> 

													<p>Efficiently transform an expanded array of methodologies rather than customized infomediaries.</p>
										    	</div>
										    </div>
									    </div>
								    </div>
								    <div role="tabpanel" class="tab-pane fade" id="trackable">
									    <div class="css-tab-content">
										    <div class="row">
										    	<div class="col-md-6">
										    		<img src="img/track.png" alt="">
										    	</div>

										    	<div class="col-md-6 content-text">
											    	<h3>Continually disintermediate</h3>

											    	<p>Credibly build virtual materials for fully researched paradigms. Authoritatively plagiarize long-term high-impact infomediaries with goal-oriented core competencies. Conveniently visualize process-centric applications through leading-edge testing procedures. Quickly build backend customer service for real-time metrics.</p> 

													<p>Efficiently transform an expanded array of methodologies rather than customized infomediaries.</p>
										    	</div>
										    </div>
									    </div>
								    </div>
								    <div role="tabpanel" class="tab-pane fade" id="fast">
									    <div class="css-tab-content">
										    <div class="row">
										    	<div class="col-md-6">
										    		<img src="img/fast.png" alt="">
										    	</div>

										    	<div class="col-md-6 content-text">
											    	<h3>Continually disintermediate</h3>

											    	<p>Credibly build virtual materials for fully researched paradigms. Authoritatively plagiarize long-term high-impact infomediaries with goal-oriented core competencies. Conveniently visualize process-centric applications through leading-edge testing procedures. Quickly build backend customer service for real-time metrics.</p> 

													<p>Efficiently transform an expanded array of methodologies rather than customized infomediaries.</p>
										    	</div>
										    </div>
									    </div>
								    </div>
								    <div role="tabpanel" class="tab-pane fade" id="reliable">
									    <div class="css-tab-content">
										    <div class="row">
										    	<div class="col-md-6">
										    		<img src="img/reliable.png" alt="">
										    	</div>

										    	<div class="col-md-6 content-text">
											    	<h3>Continually disintermediate</h3>

											    	<p>Credibly build virtual materials for fully researched paradigms. Authoritatively plagiarize long-term high-impact infomediaries with goal-oriented core competencies. Conveniently visualize process-centric applications through leading-edge testing procedures. Quickly build backend customer service for real-time metrics.</p> 

													<p>Efficiently transform an expanded array of methodologies rather than customized infomediaries.</p>
										    	</div>
										    </div>
									    </div>
								    </div>
								  </div>
								</div>
			              	</div>
			              </div>

			        	</div>
			        </section>
			       
					<section class="testimonial-section section-padding">
			            <div class="container text-center">
			              <div class="row">
			                <div class="col-xs-12">
			                  <h2 class="section-title">We are Trusted By</h2>
			                  <span class="section-sub">Monotonectally innovate cross-media<br> resources without seamless</span>
			                </div>
			              </div> <!-- /.row -->

			              <div class="row row-content">
			              	<div class="col-md-12">
								<div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
								  <!-- Indicators -->
								  <ol class="carousel-indicators">
								    <li data-target="#testimonial-carousel" data-slide-to="0" class="active"></li>
								    <li data-target="#testimonial-carousel" data-slide-to="1"></li>
								    <li data-target="#testimonial-carousel" data-slide-to="2"></li>
								  </ol>

								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
								    <div class="item active">
								    	<div class="testimonial-content">
								    		<p>Progressively architect prospective imperatives through competitive communities. Professionally productize user<br> friendly networks with out-of-the-box mindshare. Energistically incubate functionalized best practices after 24/365<br> information. Dramatically restore end-to-end strategic theme areas.</p>
								    		<span class="client-title">- Deanna Lewis -</span>
								    	</div><!-- /.testimonial-content -->
								    </div>
								    <div class="item">
								    	<div class="testimonial-content">
								    		<p>Progressively architect prospective imperatives through competitive communities. Professionally productize user<br> friendly networks with out-of-the-box mindshare. Energistically incubate functionalized best practices after 24/365<br> information. Dramatically restore end-to-end strategic theme areas.</p>
								    		<span class="client-title">- Deanna Lewis -</span>
								    	</div><!-- /.testimonial-content -->
								    </div>
								    <div class="item">
								    	<div class="testimonial-content">
								    		<p>Progressively architect prospective imperatives through competitive communities. Professionally productize user<br> friendly networks with out-of-the-box mindshare. Energistically incubate functionalized best practices after 24/365<br> information. Dramatically restore end-to-end strategic theme areas.</p>
								    		<span class="client-title">- Deanna Lewis -</span>
								    	</div>
								    </div>
								  </div>
								</div>
			              	</div>
			              </div>

			              <hr>

			              <div class="partner-section">
				              <div class="row row-content">
				              	<h2> featured usa merchants </h2>
				              	<div class="col-md-12">
									<div class="owl-carousel partner-carousel">
									    <div class="item">
									    	<a href="#"><img src="img/partner/amazon.png" alt="AMAZON"></a>
									    </div>
									    <div class="item">
									    	<a href="#"><img src="img/partner/DG.png" alt="DOLLAR GENERAL"></a>
									    </div>
									    <div class="item">
									    	<a href="#"><img src="img/partner/drugstore.png" alt="DRUGSTORE"></a>
									    </div>
									    <div class="item">
									    	<a href="#"><img src="img/partner/ebay.png" alt="EBAY"></a>
									    </div>
									    <div class="item">
									    	<a href="#"><img src="img/partner/macy.png" alt="MACY"></a>
									    </div>
									    <div class="item">
									    	<a href="#"><img src="img/partner/pet.png" alt="PETCO"></a>
									    </div>
									    <div class="item">
									    	<a href="#"><img src="img/partner/TCP.png" alt="THE CHILDREN'S PLACE"></a>
									    </div>
									    <div class="item">
									    	<a href="#"><img src="img/partner/Tdir.png" alt="Tiger Direct"></a>
									    </div>
									    <div class="item">
									    	<a href="#"><img src="img/partner/AAP.png" alt="ADVANCE AUTO PARTS"></a>
									    </div>
									</div>

							        <div class="partner-carousel-navigation">
							            <a class="prev"><i class="fa fa-angle-left"></i></a>
							            <a class="next"><i class="fa fa-angle-right"></i></a>
							        </div>
				              	</div>
				              </div>
			              </div>
			            </div>
			        </section>
			        
			        <section class="counter-section" data-stellar-background-ratio="0.5">
			        	<div class="container">
							<div class="row">
						        <div class="col-sm-4 col-xs-12">
						          <div class="counter-block">
						          	<span class="count-description flaticon-boat"><strong class="timer">799</strong>order delivered</span>
						          </div>
						        </div> 
						       <div class="col-sm-4 col-xs-12">
						          <div class="counter-block">
						          	<span class="count-description flaticon-international"><strong class="timer">19</strong>order delivered</span>
						          </div>
						        </div> 
						       <div class="col-sm-4 col-xs-12">
						          <div class="counter-block">
						          	<span class="count-description flaticon-compass"><strong class="timer">521</strong>order delivered</span>
						          </div>
						        </div> 
					      	</div> 
			        	</div>
			        </section>
			       
			        <section class="cta-section">
			        	<div class="container text-center">
			        		<a data-toggle="modal" data-target="#quoteModal" href="#" class="btn btn-primary quote-btn">Get a Quote</a>

							<!-- Modal -->
							<div class="modal fade" id="quoteModal" tabindex="-1" role="dialog" aria-labelledby="quoteModalLabel" aria-hidden="true">
							  <div class="modal-dialog modal-lg">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h3 class="modal-title" id="quoteModalLabel">Request a rate for the shipping of your goods.</h3>
							      </div>
							      <div class="modal-body">
									<form id="contactForm" action="sendemail.php" method="POST">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
												    <label for="name">Name</label>
												    <input id="name" name="name" type="text" class="form-control"  required="" placeholder="">
												</div>
											</div>
											<div class="col-md-6">
											  <div class="form-group">
											    <label for="company">Company Name</label>
											    <input id="company" name="company" type="text" class="form-control" placeholder="">
											  </div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
											  <div class="form-group">
											    <label for="phone">Phone Number</label>
											    <input id="phone" name="phone" type="text" class="form-control" placeholder="">
											  </div>
											</div>
											<div class="col-md-6">
											  <div class="form-group">
											    <label for="email">Email address</label>
											    <input id="email" name="email" type="email" class="form-control" required="" placeholder="">
											  </div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
											  <div class="form-group">
											    <label for="city">City Name</label>
											    <input id="city" name="city" type="text" class="form-control" placeholder="">
											  </div>
											</div>
											<div class="col-md-6">
											  <div class="form-group">
											    <label for="subject">Subject</label>
											    <input id="subject" name="subject" type="text" class="form-control" required="" placeholder="">
											  </div>
											</div>
										</div>
										<div class="form-group text-area">
											<label for="message">Your Message</label>
											<textarea id="message" name="message" class="form-control" rows="6" required="" placeholder=""></textarea>
										</div>

										<button type="submit" class="btn btn-primary">Send Message</button>
									</form>
							      </div>
							    </div>
							  </div>
							</div>

			        	</div><!-- /.container -->
			        </section>
			       
			       <?php
				       include ("includes/footer.php");
				    ?>
	    		</div>
		    </div>
			
	    	<?php
		       include ("includes/footer1.php");
		    ?>
		</div>

		<div id="preloader">
			<div id="status">
				<div class="status-mes"></div>
			</div>
		</div>
		
	    <!-- jQuery -->
	    <script src="js/jquery.js"></script>
	    <!-- Bootstrap Core JavaScript -->
	    <script src="js/bootstrap.min.js"></script>
	    <!-- owl.carousel -->
	    <script src="owl.carousel/owl.carousel.min.js"></script>
	    <!-- Magnific-popup -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<!-- Offcanvas Menu -->
		<script src="js/hippo-offcanvas.js"></script>
		<!-- inview -->
		<script src="js/jquery.inview.min.js"></script>
		<!-- stellar -->
		<script src="js/jquery.stellar.js"></script>
		<!-- countTo -->
		<script src="js/jquery.countTo.js"></script>
		<!-- classie -->
		<script src="js/classie.js"></script>
		<!-- selectFx -->
		<script src="js/selectFx.js"></script>
		<!-- sticky kit -->
		<script src="js/jquery.sticky-kit.min.js"></script>
	    <!-- GOGLE MAP -->
	    <script src="https://maps.googleapis.com/maps/api/js"></script>
	    <!--TWITTER FETCHER-->
	    <script src="js/twitterFetcher_min.js"></script>
	    <!-- Custom Script -->
	    <script src="js/scripts.js"></script>
	    <!-- tracking Script -->
	    <script src="js/tracking.js"></script>
	</body>
</html>
